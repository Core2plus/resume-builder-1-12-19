-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2018 at 01:39 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `orb`
--

-- --------------------------------------------------------

--
-- Table structure for table `certi_tb`
--

CREATE TABLE `certi_tb` (
  `certi_id` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `certi_name` varchar(200) DEFAULT NULL,
  `certi_insti` varchar(200) DEFAULT NULL,
  `certi_strdate` varchar(20) DEFAULT NULL,
  `certi_enddate` varchar(50) DEFAULT NULL,
  `certi_description` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certi_tb`
--

INSERT INTO `certi_tb` (`certi_id`, `master_id`, `certi_name`, `certi_insti`, `certi_strdate`, `certi_enddate`, `certi_description`) VALUES
(8, 68, 'sda', 'x', '0000-00-00', '0000-00-00', 'x'),
(9, 70, 'Android Application Developer', 'Iqra University', '2017-10-31', '01-03-2017', 'Teach Basic knowledge in android'),
(10, 70, 'C++ and Microsoft office', 'Computer Collegiate', '2018-12-31', '01-03-2012', 'asdasda');

-- --------------------------------------------------------

--
-- Table structure for table `edu_tb`
--

CREATE TABLE `edu_tb` (
  `edu_id` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `edu_name` varchar(200) NOT NULL,
  `edu_inst` varchar(200) NOT NULL,
  `edu_strdate` varchar(50) NOT NULL,
  `edu_enddate` varchar(50) NOT NULL,
  `edu_description` varchar(500) NOT NULL,
  `edu_cgpa` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `edu_tb`
--

INSERT INTO `edu_tb` (`edu_id`, `master_id`, `edu_name`, `edu_inst`, `edu_strdate`, `edu_enddate`, `edu_description`, `edu_cgpa`) VALUES
(30, 68, 'Master in computer Science', 'dasd', '2017-12-31', '2018-12-31', 'good', 0),
(31, 68, 'Bachelor in computer science', 'dasd', '2017-12-30', '2018-01-01', 'good', 0),
(36, 73, 'asda', 'da', '2017-12-31', '2018-07-31', 'asdas', 0),
(50, 70, 'Bachelor in (CS)', 'Iqra University', '01-01-2015', 'In Progress', 'Php Developer Expert', 0),
(51, 70, 'DAE in (CIT)', 'Aligarh Institute Of Technology', '01-01-2012', '01-01-2014', 'Basic knowledge of computer architecture and programming. ', 0),
(55, 70, 'Intermediate', 'Comprehensive Boys College', '01-01-2010', '01-01-2011', 'Basic information', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_tb`
--

CREATE TABLE `exp_tb` (
  `exp_id` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `exp_designation` varchar(200) DEFAULT NULL,
  `exp_company` varchar(200) DEFAULT NULL,
  `exp_strdate` varchar(50) DEFAULT NULL,
  `exp_enddate` varchar(50) DEFAULT NULL,
  `exp_description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exp_tb`
--

INSERT INTO `exp_tb` (`exp_id`, `master_id`, `exp_designation`, `exp_company`, `exp_strdate`, `exp_enddate`, `exp_description`) VALUES
(11, 73, 'das1', 'sa', '0000-00-00', '0000-00-00', 'd'),
(12, 73, 'das12', 'sa', '0000-00-00', '0000-00-00', 'a'),
(18, 70, 'Software Developer', 'Core2Plus', '25-03-2018', 'Still Working', 'HTML,PHP,Bootstrap,JaveScript,JQuery'),
(19, 70, 'Web Developer', 'Cloud Fish', '01-09-2016', '01-01-2018', 'HTML,BootStrap,CSS'),
(20, 70, 'CSR', 'Cenit', '01-06-2015', '01-01-2017', 'Customer Sale Representative');

-- --------------------------------------------------------

--
-- Table structure for table `hobby_tb`
--

CREATE TABLE `hobby_tb` (
  `hobby_id` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `hobby_names` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby_tb`
--

INSERT INTO `hobby_tb` (`hobby_id`, `master_id`, `hobby_names`) VALUES
(1, 59, 'Cricket'),
(2, 59, 'Outing'),
(3, 60, 'Cricket'),
(4, 60, 'Outing'),
(19, 70, 'Cricket'),
(20, 70, 'Football'),
(21, 70, 'Cricket'),
(22, 70, 'kacahi');

-- --------------------------------------------------------

--
-- Table structure for table `lang_tb`
--

CREATE TABLE `lang_tb` (
  `lang_id` int(11) NOT NULL,
  `lang_name` varchar(200) DEFAULT NULL,
  `lang_percentage` varchar(200) DEFAULT NULL,
  `master_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lang_tb`
--

INSERT INTO `lang_tb` (`lang_id`, `lang_name`, `lang_percentage`, `master_id`) VALUES
(112, 'English', '70', 70),
(113, 'Urdu', '60', 70);

-- --------------------------------------------------------

--
-- Table structure for table `level_table`
--

CREATE TABLE `level_table` (
  `level_id` int(11) NOT NULL,
  `level_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level_table`
--

INSERT INTO `level_table` (`level_id`, `level_name`) VALUES
(1, 'Expert'),
(2, 'Entry level'),
(3, 'Trainee');

-- --------------------------------------------------------

--
-- Table structure for table `master_tb`
--

CREATE TABLE `master_tb` (
  `master_id` int(11) NOT NULL,
  `reg_id` int(11) DEFAULT NULL,
  `title_id` int(11) DEFAULT NULL,
  `ms_image` blob,
  `ms_profile` varchar(500) DEFAULT NULL,
  `ms_fname` varchar(100) DEFAULT NULL,
  `ms_lname` varchar(100) DEFAULT NULL,
  `ms_email` varchar(100) DEFAULT NULL,
  `ms_contact` varchar(100) DEFAULT NULL,
  `ms_address` varchar(200) DEFAULT NULL,
  `ms_gender` varchar(50) DEFAULT NULL,
  `ms_dob` varchar(50) DEFAULT NULL,
  `ms_postal` int(10) NOT NULL,
  `ms_city` varchar(100) NOT NULL,
  `ms_discription` varchar(500) NOT NULL,
  `aprove` int(2) NOT NULL,
  `ms_rollno` int(100) NOT NULL,
  `ms_mname` varchar(100) NOT NULL,
  `ms_social` varchar(100) NOT NULL,
  `ms_expertise` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_tb`
--

INSERT INTO `master_tb` (`master_id`, `reg_id`, `title_id`, `ms_image`, `ms_profile`, `ms_fname`, `ms_lname`, `ms_email`, `ms_contact`, `ms_address`, `ms_gender`, `ms_dob`, `ms_postal`, `ms_city`, `ms_discription`, `aprove`, `ms_rollno`, `ms_mname`, `ms_social`, `ms_expertise`) VALUES
(57, 13, 1, 0x3135323835333636383930352e2d50554d412d4c6f676f2e6a7067, 'lorem ipsum', 'ali', 'syed', 'syed@gmail.com', '03003053530', 'karachi', 'male', '2018-04-02', 0, '', '', 1, 0, '', '', ''),
(62, 12, 1, 0x31353238363538373637, 'asdas', 'dasd', 'dasda', 'dasad@gmail.com', '03122912921', 'dasd', 'male', '2017-10-30', 0, '', '', 1, 0, '', '', ''),
(70, 15, NULL, 0x3135333037373837333433353838343238355f313732343834303038373631313833375f323034353439343639373435393138373731325f6e2e6a7067, NULL, 'Syed', 'Saad', 'syedalisaad488@gmail.com', '03122912921', 'b-12,sharbano', NULL, '12-05-2017', 750740, 'karachi                                       ', 'Make my own software house and setup institute for those who can not be able to study modern education for the future of Pakistan and make Pakistan proud.\r\n', 1, 16546, 'saad', 'https://www.linkedin.com/in/syed-ali-341b0110a/', 'Php Developer Expert'),
(73, 15, NULL, 0x31353239353735333538, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '', '', 0, 0, '', '', ''),
(75, 15, 0, 0x31353239353736343132, NULL, 'syed', 'ALI1', 'dasad@gmail.com', '03122', 'DDAS', NULL, '2018-12-31', 45, 'KARACHI ', 'das', 0, 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ref_tb`
--

CREATE TABLE `ref_tb` (
  `ref_id` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `ref_name` varchar(200) DEFAULT NULL,
  `ref_phone` varchar(200) DEFAULT NULL,
  `ref_email` varchar(200) DEFAULT NULL,
  `ref_desg` varchar(500) DEFAULT NULL,
  `ref_instit` varchar(200) DEFAULT NULL,
  `ref_address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_tb`
--

INSERT INTO `ref_tb` (`ref_id`, `master_id`, `ref_name`, `ref_phone`, `ref_email`, `ref_desg`, `ref_instit`, `ref_address`) VALUES
(10, 70, 'saad', '03122912921', 'S@GMAIL.COM', 'dasdas', 'das', ''),
(11, 70, 'SAAD', '03122', 'S@GMAIL.COM', 'SAAD', 'DASDA', 'dasda');

-- --------------------------------------------------------

--
-- Table structure for table `registeration`
--

CREATE TABLE `registeration` (
  `reg_id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `reg_username` varchar(200) DEFAULT NULL,
  `reg_contact` varchar(200) DEFAULT NULL,
  `reg_email` varchar(200) DEFAULT NULL,
  `reg_password` varchar(200) DEFAULT NULL,
  `reg_image` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registeration`
--

INSERT INTO `registeration` (`reg_id`, `role_id`, `reg_username`, `reg_contact`, `reg_email`, `reg_password`, `reg_image`) VALUES
(12, NULL, 'Sanaullah', '03003128454', 'admn@gmail.com', '@123', 0x3135323835333435353933355f3338335f333232385f3039315f463130202831292e6a7067),
(13, NULL, 'Syed Ali', '03122123123', 'syed@gmail.com', 'syed', 0x3135323835333436323264306633653533303365663862623933366665636238663065383766323564642d2d6b69642d636c6f7468696e672d62616e6e65722d64657369676e2e6a7067),
(14, NULL, 'Core2plus', '021-750383', 'core2plus@gmail.com', 'core2', 0x313532383533373130326332702d6c6f676f2e706e67),
(15, NULL, 'syed', 'ali', 'syedalisaad488@gmail.com', '123', 0x31353238353739373532627573696e6573732e6a7067),
(16, NULL, 'syed', 'ali', 'abc@gmail.com', '123', 0x3135323836313237313932323038393936315f323032323130313134343733333031385f313231383132373631343332393437363735385f6e2e6a7067),
(17, NULL, 'admin', '03122912921', 'syedalisaad488@gmail.com', '123', 0x3135323934383038333133353339343230335f323433333832313731333330323438315f333238363731323239303036313035383034385f6e2e6a7067),
(18, NULL, 'admin', '03122912921', 'admin@gmail.com', '123', 0x3135323934383039373633353439363438335f323433333832313331333330323532315f313532363035323130313332313339323132385f6e2e6a7067),
(19, NULL, 'baby', '03122912921', 'baby@gmail.com', '123', 0x3135323939393934303133353439363438335f323433333832313331333330323532315f313532363035323130313332313339323132385f6e2e6a7067),
(20, NULL, 'Admin', '03122912921', 'a@gmail.com', '123', 0x3135333030313039323433353437373133325f3438333836353534353337393139385f343030393332363238363931313536393932305f6e2e6a7067),
(21, NULL, 'Admin', '03122912921', 'admin@gmail.com', 'admin123', 0x31353330333538323731547769672d4761735f53656e736f722e626d70);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(200) DEFAULT NULL,
  `role_status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skills_tb`
--

CREATE TABLE `skills_tb` (
  `skill_id` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `skill_name` varchar(200) DEFAULT NULL,
  `skill_percentage` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills_tb`
--

INSERT INTO `skills_tb` (`skill_id`, `master_id`, `skill_name`, `skill_percentage`) VALUES
(4, 70, 'BootStrap', '60'),
(5, 70, 'HTML', '70'),
(6, 70, 'CSS', '80'),
(7, 70, 'PHP Core', '90'),
(8, 70, 'PHP  Codeigniter', '50');

-- --------------------------------------------------------

--
-- Table structure for table `subtitle_table`
--

CREATE TABLE `subtitle_table` (
  `subtitle_id` int(11) NOT NULL,
  `title_id` int(11) DEFAULT NULL,
  `subtitle_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subtitle_table`
--

INSERT INTO `subtitle_table` (`subtitle_id`, `title_id`, `subtitle_name`) VALUES
(1, 1, 'PHP'),
(2, 1, 'Database'),
(3, 2, 'Software'),
(4, 1, 'Java'),
(5, 2, 'SEO Enginner');

-- --------------------------------------------------------

--
-- Table structure for table `title_table`
--

CREATE TABLE `title_table` (
  `title_id` int(11) NOT NULL,
  `title_name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `title_table`
--

INSERT INTO `title_table` (`title_id`, `title_name`) VALUES
(0, NULL),
(1, 'Developer'),
(2, 'PHP Expert Developer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certi_tb`
--
ALTER TABLE `certi_tb`
  ADD PRIMARY KEY (`certi_id`);

--
-- Indexes for table `edu_tb`
--
ALTER TABLE `edu_tb`
  ADD PRIMARY KEY (`edu_id`),
  ADD KEY `edu_id` (`edu_id`);

--
-- Indexes for table `exp_tb`
--
ALTER TABLE `exp_tb`
  ADD PRIMARY KEY (`exp_id`);

--
-- Indexes for table `hobby_tb`
--
ALTER TABLE `hobby_tb`
  ADD PRIMARY KEY (`hobby_id`);

--
-- Indexes for table `lang_tb`
--
ALTER TABLE `lang_tb`
  ADD PRIMARY KEY (`lang_id`);

--
-- Indexes for table `level_table`
--
ALTER TABLE `level_table`
  ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `master_tb`
--
ALTER TABLE `master_tb`
  ADD PRIMARY KEY (`master_id`),
  ADD UNIQUE KEY `ms_expertise` (`master_id`);

--
-- Indexes for table `ref_tb`
--
ALTER TABLE `ref_tb`
  ADD PRIMARY KEY (`ref_id`);

--
-- Indexes for table `registeration`
--
ALTER TABLE `registeration`
  ADD PRIMARY KEY (`reg_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `skills_tb`
--
ALTER TABLE `skills_tb`
  ADD PRIMARY KEY (`skill_id`);

--
-- Indexes for table `subtitle_table`
--
ALTER TABLE `subtitle_table`
  ADD PRIMARY KEY (`subtitle_id`);

--
-- Indexes for table `title_table`
--
ALTER TABLE `title_table`
  ADD PRIMARY KEY (`title_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `certi_tb`
--
ALTER TABLE `certi_tb`
  MODIFY `certi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `edu_tb`
--
ALTER TABLE `edu_tb`
  MODIFY `edu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `exp_tb`
--
ALTER TABLE `exp_tb`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `hobby_tb`
--
ALTER TABLE `hobby_tb`
  MODIFY `hobby_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `lang_tb`
--
ALTER TABLE `lang_tb`
  MODIFY `lang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `level_table`
--
ALTER TABLE `level_table`
  MODIFY `level_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `master_tb`
--
ALTER TABLE `master_tb`
  MODIFY `master_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `ref_tb`
--
ALTER TABLE `ref_tb`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `registeration`
--
ALTER TABLE `registeration`
  MODIFY `reg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skills_tb`
--
ALTER TABLE `skills_tb`
  MODIFY `skill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `subtitle_table`
--
ALTER TABLE `subtitle_table`
  MODIFY `subtitle_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `title_table`
--
ALTER TABLE `title_table`
  MODIFY `title_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
