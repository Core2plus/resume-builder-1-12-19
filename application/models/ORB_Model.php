
<?php
	
Class ORB_Model extends CI_Model
{		
	public function __construct()
	{
		parent::__construct();
		//$this->load->database('orb');			
	} 

	public function register_process($data)
	{
		//$this->load->database('orb');
		$this->db->select('*');
		$this->db->where('reg_email',$data['email']);
		$check1 = $this->db->get('registeration');	
		if($check1!=null)
		{
			$target_dir = "assets/register_img/";
            $target_file = $target_dir . time().basename($_FILES["image"]["name"]);
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
            $imgName = time().basename($_FILES["image"]["name"]);
            move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
				
				$data = array(
					'reg_username' => $data['name'],
					'reg_contact' => $data['contact_no'],
					'reg_email' => $data['email'],
					'reg_password' => $data['password'], 
					'reg_image' => $imgName
						);

			return $insert = $this->db->insert('registeration',$data);
		
			}
		
		}

		public function login_process($email,$pass)
		{
			//$this->load->database('orb');
			$this->db->select('*');

			$this->db->where('reg_email',$email);
			$this->db->where('reg_password',$pass);
			$query = $this->db->get('registeration');

			if($query->num_rows() > 0)
			{	
				$result=$query->result();
				$reg_id = $result[0]->reg_id;
				
				$reg_name = $result[0]->reg_username;
				$image = $result[0]->reg_image;
				$roll = $result[0]->role_id;
				$this->session->set_userdata('logged');
				$this->session->set_userdata('reg_id',$reg_id);

				$this->session->set_userdata('reg_username',$reg_name);
				$this->session->set_userdata('reg_image',$image);
				$this->session->set_userdata('reg_roll',$roll);

				$this->db->select('*');
				$this->db->where('reg_id',$reg_id); 
				$query1 = $this->db->get('master_tb');
				if($query1->num_rows()>0)
				{
					$result1=$query1->result();
					$master_id = $result1[0]->master_id;
					$this->session->set_userdata('master_id',$master_id);
				}

				return $query;
			}
			else
			{
				return false;
			}		
		}

		public function PI_insert($data)
		{
			//$this->load->database('orb');
 		
 			  $target_dir = "assets/img/";
                    $target_file = $target_dir . time().basename($_FILES["client_image"]["name"]);
                    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                    $imgName = time().basename($_FILES["client_image"]["name"]);
                    move_uploaded_file($_FILES["client_image"]["tmp_name"], $target_file);
			
			$data = array(

				'reg_id' => $this->session->userdata('reg_id'),	
				'ms_image' => $imgName,
				'ms_rollno' => $data['rollno'],
				'ms_fname' => $data['name'],
				'ms_mname' => $data['mname'],
				'ms_lname' => $data['sec_name'],
				'ms_social' => $data['social'],
				'ms_email' => $data['email'],
				'ms_expertise' => $data['ms_expertise'],
				'ms_contact' => $data['contact'],
				'ms_address' => $data['address'],
				'ms_city' => $data['city'],
				'ms_postal' => $data['postal'],
				'ms_discription' => $data['discription'],
				'ms_program' => $data['ms_program'],
				'ms_department' => $data['ms_department'],
				'ms_gender' => $data['gender'],
				'Specialization' => $data['Specialization'],
				
				'ms_dob' => $data['date_of_birth']
					);

			$query = $this->db->insert('master_tb',$data);
			$master_id = $this->db->insert_id();

			$this->session->set_userdata('master_id',$master_id);
			if($query == TRUE)
			{
				redirect('ORB/info');
			}
		}

		public function get_info()
		{
			//$this->load->database('orb');
			$query = $this->db->get_where('master_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->result();	
		}
		//hobbies
		public function delete_infoIdhobbies($id)
		{
			$this->load->database('orb');
			$this->db->where('hobby_id',$id);
			$query = $this->db->delete('hobby_tb');
			return $query;
		}
		public function get_infohobbies($id)
		{
			//$this->load->database('orb');
			$query = $this->db->get_where('hobby_tb', array('master_id' => $id));	
			return $query->result();	
		}

		public function get_infoIdhobbies($id)
		{
			//echo $id;
			$this->load->database('orb');
			$query = $this->db->query('
       	 	select * from hobby_tb 
      		where master_id="'.$id.'"
      		');
			return $query->result();
		}
		//education
		public function delete_infoIdedu($id)
		{
			$this->load->database('orb');
			$this->db->where('edu_id',$id);
			$query = $this->db->delete('edu_tb');
			return $query;
		}

		public function get_infoedu($id)
		{  
			$this->load->database('orb');
			$query = $this->db->get_where('edu_tb', array('master_id' => $id));	
			return $query->result();	
		}

		public function get_infoIdedu($id)
		{
			echo $id;
			$this->load->database('orb');
			$query = $this->db->query('
       	 	select * from edu_tb 
      		where master_id="'.$id.'"
      		');

			return $query->result();
		}

		public function eduedit($id)
		{
			//$this->load->database('orb');
			$this->db->where('edu_id',$id);
			$query = $this->db->get('edu_tb');
			$query->result();
			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				return false;
			}
		}
				
		public function edu_editprocess($data)
		{
			//$this->load->database('orb');
			$this->db->where('edu_id',$data['edu_id']);
			$this->db->update('edu_tb',$data);
			if($this->db->affected_rows() > 0)
			{
				return true; 
			}
			else
			{
				return false;
			}
		}
 				
		//working experince 
		public function delete_infoIdwork($id)
		{
			//$this->load->database('orb');
			$this->db->where('exp_id',$id);
			$query = $this->db->delete('exp_tb');
			return $query;
		}
		
		public function get_infowork($id)
		{
			//$this->load->database('orb');
			$query = $this->db->get_where('exp_tb', array('master_id' => $id));	
			return $query->result();	
		}

		public function get_infoIdwork($id) 
		{
			//echo $id;
			$this->load->database('orb');
			$query = $this->db->query('
       	 	select * from exp_tb 
      		where master_id="'.$id.'"
      		
      		');

			return $query->result();
		} 

		//here working skill
		public function delete_infoIdskill($id)
		{
			$this->load->database('orb');
			$this->db->where('skill_id',$id);
			$query = $this->db->delete('skills_tb');
			return $query;
		}
	
		public function get_infoskill($id)
		{	
			//$this->load->database('orb');
			$query = $this->db->get_where('skills_tb', array('master_id' => $id));	

			return $query->result();	
		}

		public function get_infoIdskill($id) 
		{
			$id;
			$this->load->database('orb');
			$query = $this->db->query('
	       	 select * from skills_tb 
	      	where master_id="'.$id.'"
	      	');
			return $query->result();
		}
		/////end skill here.........
			//start .. here working lang
		public function delete_infoIdlang($id)
		{
			$this->load->database('orb');
			$this->db->where('lang_id',$id);
			$query = $this->db->delete('lang_tb');
			return $query;
		}
		
		public function get_infolang($id)
		{
			
			$this->load->database('orb');
			$query = $this->db->get_where('lang_tb', array('master_id' => $id));	

			return $query->result();	
		}
		
		public function get_infoIdlang($id) 
		{
			//echo $id;
			$this->load->database('orb');
			$query = $this->db->query('
	       	 select * from lang_tb 
	      	where master_id="'.$id.'"
	      	');
				return $query->result();
		}
		/////end lang here.........
		//.....................//.......certification..model .....//................
		public function delete_infoIdcertification($id)
		{
			$this->load->database('orb');
			$this->db->where('certi_id',$id);
			$query = $this->db->delete('certi_tb');
			return $query;
		}
		
		public function get_infocertification($id)
		{
			$this->load->database('orb');
			$query = $this->db->get_where('certi_tb', array('master_id' => $id));	
			return $query->result();	
		}

		public function get_infoIdcertification($id)  //get_infoIdcertificatin
		{
			//echo $id;
			$this->load->database('orb');
			$query = $this->db->query('
	       	 select * from certi_tb 
	      	where master_id="'.$id.'"
	      	');
			return $query->result();
		}
		//.....................//.........end.certification.model............//...............

		//education End --> Delete /////

		//reference
	
		public function get_inforeference($id)
		{
			$this->load->database('orb');
			$query = $this->db->get_where('ref_tb', array('master_id' => $id));	
			return $query->result();	
		}

		public function get_infoIdreference($id)
		{
			$this->load->database('orb');		
			$query = $this->db->query('
       	 	select * from ref_tb 
      		where master_id="'.$id.'"	
      		');
			return $query->result();
		}

		public function workedit($id)
		{
			$this->load->database('orb');
			$this->db->where('exp_id',$id);
			$query = $this->db->get('exp_tb');
			$query->result();
			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				return false;
			}
		}
			
		public function work_editprocess($data)
		{
			$this->load->database('orb');
			$this->db->where('exp_id',$data['exp_id']);
			$this->db->update('exp_tb',$data);
			if($this->db->affected_rows() > 0)
			{
				return true; 
			}
			else
			{
				return false;
			}
		}
		//????????????// exp edit end here>>>>>>>>>>>>>>>>>>>
 				
//...................... certification model edit here..........................................

		public function certificationedit($id)
		{
			$this->load->database('orb');
			$this->db->where('certi_id',$id);
			$query = $this->db->get('certi_tb');
			$query->result();
			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				return false;
			}
		}

		public function certification_editprocess($data)
		{
			$this->load->database('orb');
			$this->db->where('certi_id',$data['certi_id']);
			$this->db->update('certi_tb',$data);
			if($this->db->affected_rows() > 0)
			{
				return true; 
			}
			else
			{
				return false;
			}
		}

//-------------------------------------certification model edit here....................................		

//----------- reference model edit start here .............
		//reference
		public function delete_infoIdreference($id)
		{
			$this->load->database('orb');
			$this->db->where('ref_id',$id);
			$query = $this->db->delete('ref_tb');
			return $query;
		}

		public function referenceedit($id)
		{
			$this->load->database('orb');
			$this->db->where('ref_id',$id);
			$query = $this->db->get('ref_tb');
			$query->result();
			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				return false;
			}
		}
		
		public function reference_editprocess($data)
		{
			$this->load->database('orb');
			$this->db->where('ref_id',$data['ref_id']);
			$this->db->update('ref_tb',$data);
			if($this->db->affected_rows() > 0)
			{
				return true; 
			}
			else
			{
				return false;
			}
		}

//----------- rererence model edit end there ................
		//working experince 
		public function get_infoId($id)
		{
			//echo $id;
			$this->load->database('orb');
			$query = $this->db->query('
       	 	select master_tb.* from master_tb 
        	where master_id="'.$id.'"
      		
      		');
			return $query->result();
		}

		public function allTitles()
		{
			$query=$this->db->query("select * from title_table");
			return $query->result();
		}

		public function allLevels()
		{
			$query = $this->db->query('select * from level_table');
			return $query->result();
		}
		
		public function delete_infoId($id)
		{
			$this->load->database('orb');
			$this->db->where('master_id',$id);
			$query = $this->db->delete('master_tb');
			return $query;
		}

		public function edit()
		{

			//$this->load->database('orb');
			//$query = $this->db->get('master_tb');
			//$this->db->where('master_id',$this->session->userdata('master_id'));

			$query = $this->db->get_where('master_tb', array('master_id' => $this->session->userdata('master_id') ));

			$query->result_array();

			

			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				return false;
			}
		}
		public function PI_department()
		{
			$this->load->database('orb');
			$this->db->select('dapt_name');

			$query = $this->db->get('dapartment');


			return $query;

		}
	
		public function PI_editprocess($data)
		{
			$this->load->database('orb');
 			$id = $this->input->post('text_hide');

 			  $target_dir = "assets/img/";
                    $target_file = $target_dir . time().basename($_FILES["client_image"]["name"]);
                    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                    $imgName = time().basename($_FILES["client_image"]["name"]);
                    move_uploaded_file($_FILES["client_image"]["tmp_name"], $target_file);
			
			$data1 = array(

			'reg_id' => $this->session->userdata('reg_id'),
				'ms_image' => $imgName,
				'ms_rollno' => $data['rollno'],
				'ms_fname' => $data['name'],
				'ms_mname' => $data['mname'],
				'ms_lname' => $data['sec_name'],
				'ms_social' => $data['social'],
				'ms_expertise' => $data['ms_expertise'],
				'ms_email' => $data['email'],
				'ms_program' => $data['ms_program'],
				'ms_department' => $data['ms_department'],
				'Specialization' => $data['Specialization'],
				'ms_contact' => $data['contact'],
				'ms_address' => $data['address'],
				'ms_city' => $data['city'],
				'ms_postal' => $data['postal'],
				'ms_discription' => $data['discription'],
				'ms_gender' => $data['gender'],
				'ms_dob' => $data['date_of_birth']
				);
			

			$this->db->where('master_id',$this->session->userdata('master_id'));
			$this->db->update('master_tb',$data1);
		
			if($this->db->affected_rows() > 0)
			{
				return true; 
			}
			else
			{
				return false;
			}
		}

		public function get_infodash()
		{
			$this->load->database('orb');
			$query = $this->db->get_where('master_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->row();	
		}

		public function get_infodash2()
		{
			$this->load->database('orb');
			$query = $this->db->get_where('master_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->row();	
		}
		//rk chnages
		
		public function get_infodash3()
		{
		
			$this->load->database('orb');
			$id=$this->uri->segment(3);
			$query = $this->db->query('
		       	 	select master_tb.* from master_tb 
      				where master_id="'.$id.'"');	
			return $query->row();	
		
		}

		//rk end changes
		public function get_expdash()
		{
			$this->load->database('orb');
			$query = $this->db->get_where('exp_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->row();	
		}

		public function get_expdash3()
		{
			$this->load->database('orb');
			$id=$this->uri->segment(3);
			$query = $this->db->get_where('exp_tb', array('master_id' => $id));	
			return $query->result();	
		}

		public function get_edudash3()
		{
			$this->load->database('orb');
			$id=$this->uri->segment(3);
			$query = $this->db->get_where('edu_tb', array('master_id' => $id));	
			return $query->result();	
		}


		public function get_certidash3()
		{
			$this->load->database('orb');
			$id=$this->uri->segment(3);
			$query = $this->db->get_where('certi_tb', array('master_id' => $id));	
			return $query->result();	
		}

		public function get_langdash3()
		{
			$this->load->database('orb');
			$id=$this->uri->segment(3);
			$query = $this->db->get_where('lang_tb', array('master_id' => $id));	
			return $query->result();	
		}

		public function get_refdash3()
		{
			$this->load->database('orb');
			$id=$this->uri->segment(3);
			$query = $this->db->get_where('ref_tb', array('master_id' => $id));	
			return $query->result();	
		}

		public function get_skilldash3()
		{
			$this->load->database('orb');
			$id=$this->uri->segment(3);
			$query = $this->db->get_where('skills_tb', array('master_id' => $id));	
			return $query->result();	
		}

		public function get_hobbydash3()
		{
			$this->load->database('orb');
			$id=$this->uri->segment(3);
			$query = $this->db->get_where('hobby_tb', array('master_id' => $id));	
			return $query->result();	
		}

		public function get_edudash()
		{
			$this->load->database('orb');
			$query = $this->db->get_where('edu_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->row();	
		}

		public function get_certidash()
		{
			$this->load->database('orb');
			$query = $this->db->get_where('certi_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->row();	
		}
		
		public function get_langdash()
		{
			$this->load->database('orb');
			$query = $this->db->get_where('lang_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->row();	
		}

		public function get_refdash()
		{
			$this->load->database('orb');
			//$this->db->distinct('master_id');
			$query = $this->db->get_where('ref_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->row();	
		}


		public function get_skilldash()
		{
			$this->load->database('orb');
			//$this->db->distinct('master_id');
			$query = $this->db->get_where('skills_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->row();	
		}

		public function get_hobbydash()
		{
			$this->load->database('orb');
			//$this->db->distinct('master_id');
			$query = $this->db->get_where('hobby_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->row();	
		}

		public function get_infodash1()
		{
			$this->load->database('orb');
			$id = $this->session->userdata('master_id');
			$query = $this->db->query('
		       	 	select master_tb.*  from master_tb where master_id="'.$id.'"');			
			return $query->row();	
		}

		public function get_expdash1()
		{
			$this->load->database('orb');
			$query = $this->db->get_where('exp_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->result();	
		}

		public function get_edudash1()
		{
			$this->load->database('orb');
			$query = $this->db->get_where('edu_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->result();	
		}

		public function get_certidash1()
		{
			$this->load->database('orb');
			$query = $this->db->get_where('certi_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->result();	
		}
		
		public function get_langdash1()
		{
			$this->load->database('orb');
			$query = $this->db->get_where('lang_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->result();	
		}

		public function get_refdash1()
		{
			$this->load->database('orb');
			//$this->db->distinct('master_id');
			$query = $this->db->get_where('ref_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->result();	
		}

		public function get_skilldash1()
		{
			$this->load->database('orb');
			//$this->db->distinct('master_id');
			$query = $this->db->get_where('skills_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->result();	
		}

		public function get_hobbydash1()
		{
			$this->load->database('orb');
			//$this->db->distinct('master_id');
			$query = $this->db->get_where('hobby_tb', array('master_id' => $this->session->userdata('master_id')));	
			return $query->row();	
		}
	
		//resume
		public function get_inforesume($id)
		{
			$this->load->database('orb');
			return $this->db->get_where('master_tb',['master_id'=>$id],1)->row_array();
			/*$query = $this->db->query('
       	 	select master_tb.* ,edu_tb.*,exp_tb.*,certi_tb.*,ref_tb.*,skills_tb.*,title_table.title_name,subtitle_table.subtitle_name , level_table.level_name from master_tb 
        	LEFT JOIN `title_table`  ON 
          	`title_table`.title_id=master_tb.title_id
          		LEFT JOIN `certi_tb`  ON 
          	`certi_tb`.master_id=master_tb.master_id
          	LEFT JOIN `ref_tb`  ON 
          	`ref_tb`.master_id=master_tb.master_id
          	LEFT JOIN `skills_tb`  ON 
          	`skills_tb`.master_id=master_tb.master_id
          	LEFT JOIN `edu_tb`  ON 
          	`edu_tb`.master_id=master_tb.master_id
          	LEFT JOIN `exp_tb`  ON 
          	`exp_tb`.master_id=master_tb.master_id
        	LEFT JOIN `subtitle_table` ON
          	`subtitle_table`.subtitle_id=master_tb.subtitle_id
        	LEFT JOIN `level_table` ON
          	`level_table`.level_id=master_tb.level_id
      		where master_tb.master_id="'.$id.'"
      		
      		');	
*/
			return $query->result();	
		}
		
	public function adminshow()
	{
		$this->load->database('orb');
		$query = $this->db->query('select * from master_tb');
		return $query->result();	
	}

	public function approveshow()
	{
		$this->load->database('orb');
		$query = $this->db->query('select * from master_tb where aprove = 1');	
		return $query->result();	
		}

	public function pendingshow()
	{
		$this->load->database('orb');
		$query = $this->db->query('select * from master_tb where aprove = 0');	
		return $query->result();	
	}

		public function deniedshow()
		{
			$this->load->database('orb');
			$query = $this->db->query('select * from master_tb where aprove = 2');	
			return $query->result();	
		}

		public function checkapproved($id)
		{
			//$array = array('1','2');
			$this->load->database('orb');
			$this->db->select('aprove');
			//$this->db->where_in('aprove',$array);
			$this->db->where('master_id',$id);
			$query = $this->db->get('master_tb');
			return $query->result();	
		}

		public function get_eduresume($id)
		{
			$this->load->database('orb');
			$query = $this->db->get_where('edu_tb', array('master_tb' => $id));	
			return $query->result();	
		}

		public function get_masterData($id)
		{
			return $this->db->get_where('master_tb',['master_id'=>$id],1)->row_array();
		}

		public function get_educationData($id)	
		{
    		if ( ! empty($id))
    		{
        		$query = $this->db->get_where('edu_tb', array('edu_id' => $id));
    		}
    	else
    	{
        	$query = $this->db->get('edu_tb');
    	}

    	if ($query->num_rows() > 0)
    	{
        	return $query->result_array();
    	}
	}
	
	public function get_languagesData($id)
	{
		return $this->db->get_where('lang_tb',['lang_id'=>$id],3)->result_array();
	}
	
}

?>