<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Signup </title>
  <!-- CORE CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">

<link href="<?php echo base_url(); ?>assets/css/mystyle.css" rel="stylesheet" type="text/css">
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css"> -->

<style>
body  {
    background-image: url("<?php echo base_url('assets/img/myu_signin_bglogo/university-background-06.jpg') ?>");
    background-color: #cccccc;
}
</style>

  
</head>

<body style="background:#94a5c5a3;" >
<div class="container">
    <div class="company-logo"><img src="<?php echo  site_url(); ?>assets/img/iqralogo.png" height="54" width="300" class="img-responsive my-logo1" style="margin:auto;" alt=""></div>
    <h1 class="well" style="color: #002c44; font-family: serif; text-align: center; opacity: 0.5;">
    <span style="font-weight: bolder; text-shadow: 0px 3px 10px #003b5a;">Registration Form</span></h1>
  <div class="col-lg-12 well">
    <div class="row">
    <?php
            if(isset($_SESSION['error']))
            {
        ?>
            <div class="alert alert-danger">
                <?php
                    echo $_SESSION['error'];
                ?>
            </div>
        <?php
            }
        ?>
    </div>
  <div class="row">
        <form action="<?php echo base_url(); ?>ORB/register_form" method="post" accept-charset="utf-8" enctype="multipart/form-data">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-6 form-group">
                <label>Name</label>
                <input type="text" placeholder="Enter Name Here.."  required autofocus name="name" class="form-control" >
              </div>
              <div class="col-sm-6 form-group">
                <label>Contact No:</label>
                <input type="text"  pattern="^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$" placeholder="Enter Contact Here.."  required name="contact_no" class="form-control">
              </div>
            </div>                
          <div class="form-group">
            <label>Email Address</label>
            <input type="email" placeholder="Enter Email Address Here.." required name="email" class="form-control">
          </div>  
          <div class="form-group">
            <label>Password</label>
            <input type="password" placeholder="Enter Your Password Here.." required name="password" id="password" class="form-control">
          </div>
          <div class="form-group">
            <label>Confirm Password</label>
            <input type="password" placeholder="Enter Your Confirm Password Here.." name="Confirm-password" id="Confirm-password" required  class="form-control" onchange="check()">
          <span id="pass_error"></span>
          </div>
          <div class="form-group">
            <label>Profile Image *</label>
            <input type="file" required name="image" class="form-control">
          </div>
          <input type="submit" disabled="disabled" id="submit"  class="btn btn-lg btn-block btn-primary active" style="box-shadow: 0px 6px 24px grey;font-family:serif;" value="submit"><br>
          <a href="<?php echo base_url(); ?>" style="color:#465986;">Already have an acount!!</a>        
          </div>

        </form>
      
        </div>
  </div>
  </div>

    <script>
        function check()
        {
          var x=  document.getElementById('submit');
          var flag = true;
          
          var pass = document.getElementById('password').value;
          var Cpass = document.getElementById('Confirm-password').value;

          if(pass != Cpass)
          {
            document.getElementById('pass_error').innerHTML = "Password is not mached";
            document.getElementById('pass_error').style.color = "red";
            x.disabled=true;
          }
            else if(pass == Cpass)
          {
            document.getElementById('pass_error').innerHTML = "Password is mached";
            document.getElementById('pass_error').style.color = "green";
             x.disabled= false;
          }

          if(flag == true)
          {
            return true;
          }
          else
          {
            return false;
          }

        }     
    </script>
</body>
</html>