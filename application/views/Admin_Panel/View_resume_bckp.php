<!DOCTYPE html>
<html>
<head>

	<script src="https://use.fontawesome.com/0ff82497e0.js"></script>
	<title>View Resume</title>
	<style type="text/css">

/* html reset css */
html, body, div, span, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
abbr, address, cite, code,
del, dfn, em, img, ins, kbd, q, samp,
small, strong, sub, sup, var,
b, i,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section, summary,
time, mark, audio, video {margin:0;padding:0;border:0;outline:0;font-size:100%;vertical-align:baseline;
    background:transparent;}
body {line-height:1;}
article,aside,details,figcaption,figure,
footer,header,hgroup,menu,nav,section {  display:block;}
nav ul {list-style:none;}
blockquote, q {quotes:none;}
blockquote:before, blockquote:after,
q:before, q:after {content:'';content:none;}
a {margin:0; padding:0;font-size:100%;vertical-align:baseline;background:transparent;}
/* change colours to suit your needs */
ins {background-color:#ff9;color:#000;text-decoration:none;}
/* change colours to suit your needs */
mark {background-color:#ff9;color:#000;font-style:italic;font-weight:bold;}
del {text-decoration: line-through;}
abbr[title], dfn[title] {border-bottom:1px dotted;cursor:help;}
table {border-collapse:collapse;border-spacing:0;}
/* change border colour to suit your needs */
input, select { vertical-align:middle;}
input, select {vertical-align:middle;}
.clearfix:after {content: ".";display: block;clear: both;visibility: hidden;line-height: 0;
    height: 0;}
.clearfix {display: inline-block;}
 html[xmlns] .clearfix {display: block;}
 * html .clearfix {height: 1%;}

/* html reset css  End*/

@font-face{
    font-family:adnan_khan_regular;
    src:url('assets/fonts/OpenSans-Regular.ttf');
}

@font-face{
    font-family:adnan_khan_light;
    src:url('assets/fonts/OpenSans-Bold.ttf');
}



/*@media print {
body {
    background-color: none!important;
    -webkit-print-color-adjust: exact; 
}}

@media print {
    body {
    color: none !important;
}}
*/
/*.page_wrape{
    width:750px;
    margin-right: auto;
    margin-left:auto;
    margin-top:50px;
    border-top:5px solid #45b1fc;
background:#ffff;
margin-bottom:50px;
border-bottom:5px solid #ccc;
    
}*/

.page_wrape{
    width:750px;
    margin-right: auto;
    margin-left:auto;
    margin-top:10px;
    border-top:5px solid #45b1fc;
background:#ffff;
margin-bottom:10px;

}

.main_section{
    width:100%;
    background:blue;
}

aside{
width:250px;
padding-top:50px;
padding-bottom:50px;
background-color:lightgrey;
}

.right-colum{
    width:500px;
    

    
}

.fleft{
    float:left;

}
.fright{
    float:right;
}
.image-section{
    width:100%;
    
    
}
#person-image{
    width:156px;
    height:156px;
    border-radius:81px;
    border:5px solid #45b1fc;
    margin-right: auto;
    margin-left: auto;}

.links-icons{
    margin-top:20px;
    width:100%;
}
.img-circle{
 border-radius:75px;   
}

.social-icons_i{
    width:35px;
    height:35px;
    border-radius:15px;
    line-height:30px;
    
    text-align:center;
    color:#45b1fc;
}
.social-icons{
    width:100%;
    margin-bottom: 30px;
    
}
ul.social-icons li{
width:185px;
list-style:none;
margin-bottom:20px;
margin-left:auto;
margin-right:auto;
padding-right:10px;

}
ul.social-icons p{
   width:150px; 
   font-size:14px;
   color:#5e5e5e;
   font-weight:700;
   vertical-align:middle;
   display:inline-block;
   font-family:'adnan_khan_regular';
}

.profile-area{
    width:100%;
     
    margin-top: 20px;
}

.profile-area-heading3{
    width:225px;
    font-weight:700;
    text-align:center;
    margin-right: auto;
    margin-left: auto;
    font-size:22px;
    letter-spacing:3px;
    text-align:center;
    text-transform:uppercase;
    color:#414141;
    margin-bottom: 20px;
    border-bottom: 3px solid #45b1fc; 
   line-height: 0.1em;
   font-family:'adnan_khan_regular';
    }

    .profile-area-heading4{
    width:225px;
    text-align:center;
    margin-right: auto;
    margin-left: auto;
    font-size:22px;
    letter-spacing:1px;
    text-align:center;
    text-transform:uppercase;
    color:#414141;
    margin-bottom: 20px;
    border-bottom: 3px solid #45b1fc; 
   line-height: 0.1em;
   font-family:'adnan_khan_regular';
    
    }

.profile-area-heading4 span{
background:lightgrey; 
     
    }
.profile-area-heading3 span{
background:lightgrey; 
     
    }
.p1{
    width:185px;
    margin-top: -12px;
    margin-right: auto;
    margin-left: auto;
   text-align:justify;
   font-family:'adnan_khan_regular';
    font-size:12px;
    color:#6f6f6f;
}
.skill-sec{
    width: 100%;
    margin-top: 20px;
}

ul.skill-progress{
    width:220px;
    margin-right: auto;
    margin-left: auto;
}

ul.skill-progress li{
    list-style: none;
    margin-bottom:20px;
    color:#414141;
    font-size:14px;
    font-family:'adnan_khan_regular';
    font-weight:700;}
ul.skill-progress p{
    display:inline-block;
    
}

progress{
    width:120px;
    
    
}

ul.skill-progress span{
    width: 100px;
}

.right-content-area{
    width:100%;
    padding-top:50px;
    }
.main-heading h3{
    font-size:42px;
    text-align:center;
    font-family:'adnan_khan_regular';
    text-transform:uppercase;
    color:#5e5e5e;
    margin-bottom:10px;
}
.main-heading h3 span{
    font-family:'adnan_khan_light';
    color:#45b1fc;
}

.main-heading p{
   text-align:center;
    font-family:'adnan_khan_regular';
    color:#5e5e5e;
    font-size:18px;
}
 .Education-heading{
    width:450px;
    text-align:center;
    margin-right: auto;
    margin-left: auto;
    font-size:22px;
    letter-spacing:1px;
    text-align:center;
    text-transform:uppercase;
    color:#414141;
    margin-top: 30px;
    margin-bottom: 20px;
    border-bottom: 3px solid #45b1fc; 
   line-height: 0.1em;
   font-family:'adnan_khan_regular';
    
    }
.fa {
    display: inline-block;
    font: none !important;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
.Education-heading span{
background:#fff; }

.a2007{
    width:450px;
    margin-left:auto;
    margin-right:auto;
}

.Maenglish{
    width:230px;
    line-height: 1.3;
    font-size:14px;
    /*text-transform:capitalize;*/
    font-family:'adnan_khan_light';
}

 .p3{
width:220px;
font-size:14px;
text-transform:uppercase;
text-align: end;
font-family:'adnan_khan_light';
 }
.main_2007{
    width:100%;
    margin-bottom: 10px;
    
}
.p-4{
    font-family:'adnan_khan_regular';
    font-size:14px;
    line-height: 1.3;

}

ul.social-Hobies li{
    width:50px;
    height:50px;
    list-style:none;
    line-height:50px;
    background:#ccc;
    font-size:18px;
    text-align:center;
    border-radius:25px;
    display:inline-block;
   margin-left:20px;
   margin-right:39px;
   margin-bottom:10px;

}


/* Custom css Start here */
	
</style>
</head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<body>
<!--------Main Div class div starts-------->

<div class="page_wrape" >
<section class="main-section clearfix">
    <aside class="fleft">


    <div class="image-section">
 <!-- Person Image -->
   


        <div id="person-image">
        <!-- <img src="client_images/<?php echo $get_masterData['client_image']; ?>" class="img-circle" height="157" width="157" alt=""> -->
        <img 
        src="<?php echo base_url()."assets/img/".$query->ms_image; ?>"
         class="img-circle" height="157" width="157" alt="">
        </div> 
         </div>
<!--------header div end-------->


<!-- <center>
<div class="name">
</center> -->





<!--------aboutme class div start-------->
<!-- <div class="aboutme">
<h2><P>OBJECTIVE</P></h2>
<span class="aboutmecontent"   ><?php echo $query->ms_discription; ?>.</span>
</div> -->

<div class="profile-area">
    <div class="heading-profile">
    
     <h3 class='profile-area-heading3'><span>OBJECTIVE<span></h3>
    
        <p class="p1"><?php echo $query->ms_discription; ?></p>
    </div>
</div>
<!--------aboutme class div end-------->

<br>
<!--------Contactme class div start-------->

<div class="heading-profile">
    
<h3 class='profile-area-heading3'><span>CONTACT<span></h3>

<div class="links-icons">
<ul class="social-icons">
<?php
if($query->ms_social!=null){
?>
<!-- <div class="phonenumber">
<div class="icon">

<span class="glyphicon glyphicon-link " style="font-size: 20px;" ></span>
</div>
<spam class="phone"><?php echo $query->ms_social; ?></spam><br>

</div> -->
<?php
}
?>
<!-- <div class="phonenumber col-sm-12" style="padding-left: 0px; margin-bottom: 10px; width: 400px;">

<span class="glyphicon glyphicon-earphone  col-md-6" style="font-size: 20px;" ></span>
<spam  class="phone col-md-6" ><?php echo $query->ms_contact; ?><br></spam>
</div> -->

   <li><i class="fa fa-phone social-icons_i cls"></i><p>&nbsp;<?php  echo $query->ms_contact; ?></p></li>

  
   <li><i class="fa fa-envelope social-icons_i cls"></i><p>&nbsp;<?php echo $query->ms_email; ?></p></li>

<li><i class="fa fa-envelope social-icons_i cls"> </i><p>&nbsp;<?php echo $query->ms_address; ?></p></li>

   
</ul>
</div>
<!-- <div class="Email col-sm-12" style="padding-left: 0px; margin-bottom: 10px; width: 400px;">

<span class="glyphicon glyphicon-envelope col-md-6" style="font-size: 20px;" ></span>
<spam class="phone col-md-6"><?php echo $query->ms_email; ?><br></spam>
</div>



<div class="location col-sm-12"style="padding-left: 0px; margin-bottom: 10px; width: 400px;">

<span class="glyphicon glyphicon-map-marker col-md-6" style="font-size: 20px;" ></span>
<spam class="phone col-md-6"><?php echo $query->ms_address; ?>

</spam>
</div> -->

</div>
<!--------Contactme class div end-------->








<!--------Language class div Start-------->



<!--------Language class div end-------->


<!--------personalSkills class div Start-------->
	<div class="skill-sec" style="margin-top: 40px!important;">
    <div class="heading-profile">
   
       <h3  class="profile-area-heading3"><span>SKILLS</span></h3> 
  
    <ul class="skill-progress">    
    	<?php foreach($query7 as $skill1): ?>
<li><p style="width:100px"><?php echo $skill1->skill_name; ?></p><progress value="<?php echo $skill1->skill_percentage; ?>" max="100" width="20px">30%</progress></li>
<?php endforeach; ?>

</ul>
</div>
</div>




<div class="skill-sec" style="margin-top: 40px!important;">
    <div class="heading-profile">
    
     <h3 class='profile-area-heading4'><span>Languages</span></h3>
   
    <ul class="skill-progress">    
    	<?php foreach($query5 as $key): ?>
<li><p style="width:100px"><?php echo $key->lang_name; ?></p><progress value="<?php echo $key->lang_percentage; ?>" max="100" width="20px">30%</progress></li>
<?php endforeach; ?>
<!-- <li><p style="width:100px">GERMON </p><progress value="30" max="100" width="20px">30%</progress></li>
<li><p style="width:100px">PERSIAN </p><progress value="100" max="100" width="20px">30%</progress></li> -->
</ul>
</div>
</div>


</aside>
<!--------Left class div end-------->
<div class="right-colum fright">
<div class="right-content-area">
	    <div class="main-heading">
        <h3 style="text-transform: uppercase;"><?php echo $query->ms_fname." "; ?> <span><?php echo $query->ms_lname; ?></span></h3>
        <center><span style="text-transform: uppercase;"><b><?php echo $query->ms_expertise; ?></b></span></center>
    </div>



<div class="Education">
<h3 class='Education-heading'><span>EDUCATION<span></h3>
<?php foreach ($query3 as $key): ?>
<div class="a2007">
<div class="main_2007">
<div class="Maenglish fleft" style="font-weight: bold;font-family: sans-serif!important;"><?php echo $key->edu_name; ?> </div>
<span  class="p3 fright">|<?php echo $key->edu_enddate; ?></span>
<div class="Maenglish fleft" style="font-family: sans-serif!important;"><?php echo $key->edu_inst; ?></div>

<p class="p-4 Maenglish" ><?php echo $key->edu_description; ?></p>
</div>
</div>
<?php endforeach; ?>
</div>




<!--------left box div starts-------->
<div class="Education">
<h3 class='Education-heading'><span>EXPERIENCE<span></h3>
<?php foreach ($query2 as $key): ?>
<div class="a2007">
<div class="main_2007">
<div class="Maenglish fleft" style="font-weight: bold;font-family: sans-serif!important;"><?php echo $key->exp_designation; ?></div>
<span  class="p3 fright">|<?php echo $key->exp_strdate; ?>-<?php echo $key->exp_enddate; ?></span>
<div class="Maenglish fleft" style="font-family: sans-serif!important;"><?php echo $key->exp_company; ?> </div>
<span  class="p3 fright">&nbsp;</span>
<p class="p-4 Maenglish"><?php echo $key->exp_description; ?></p>
</div>
</div>
<?php endforeach; ?>
</div>
<!--  Professional Qualification-->
<!-- <div class="leftbox">
<span class=" glyphicon glyphicon-folder-close icon" aria-hidden="true"></span>
<h3 class="leftheader">WORK EXPERIENCE
<hr>
</h3>
<?php foreach ($query2 as $key): ?>
<div class="master-degree">
<span class="degree" ><?php echo $key->exp_designation; ?></span>
<input class="date" value="<?php echo $key->exp_strdate; ?> to <?php echo $key->exp_enddate; ?>" name="ProfessionalDate" disabled="true"><br>
<div width="100%"><span class="content" ><?php echo $key->exp_company; ?></span>
</div>
<div width="100%"><span class="content" ><?php echo $key->exp_description; ?></span>
</div>
</div>
<?php endforeach; ?>
</div> -->
<!--------left box div ends-------->


<!--------leftbox1 div starts-------->
<div class="Education" id="testing_skip" >
<h3 class='Education-heading'><span>CERTIFICATION<span></h3>
<?php foreach ($query4 as $key):?>
<div class="a2007">
<div class="main_2007">
<div class="Maenglish fleft" style="font-weight: bold;font-family: sans-serif!important;"><?php echo $key->certi_name; ?></div>
<span  class="p3 fright">|<?php echo $key->certi_enddate; ?></span>
<div class="Maenglish fleft" style="font-family: sans-serif!important;"><?php echo $key->certi_insti; ?> </div>
<span  class="p3 fright">&nbsp;</span>
<p class="p-4 Maenglish"><?php echo $key->certi_description; ?></p>
</div>
</div>
<?php endforeach; ?>
</div>
<!-- Work Experience-->
<!-- <div class="leftbox">
<span class="glyphicon glyphicon-cog icon" aria-hidden="true"></span>
<h3 class="leftheader">CERTIFICATION
<hr>
</h3>
<?php foreach ($query4 as $key): ?>

<div class="master-degree">
<span class="degree"> <?php echo $key->certi_name; ?></span>
<input class="date" value="<?php echo $key->certi_strdate; ?> to <?php echo $key->certi_enddate; ?>" 
name="WorkExperienceDate" disabled="true">

<div width="70%">
<span class="content" ><?php echo $key->certi_insti; ?></span>
</div>
<div width="70%">
<span class="content" ><?php echo $key->certi_description; ?></span>
</div>
</div>
<?php endforeach; ?>

</div> -->
<!--------left box div ends-------->


<!-- References-->
<!--wrench-->


<!--------left box2 div starts-------->
<div class="education">
<h3 class='Education-heading'><span>REFERENCE<span></h3>
	<table>
		<tr>
			<?php foreach($query6 as $key): ?>
			<td>
				
	<div class="a2007" style="width:50%!important;margin-left: 25px!important">
	<div class="main_2007">
	<div class="Maenglish fleft" style="text-transform: none!important;" ><span style="text-transform: none;font-weight: bold;"></span> <?php echo $key->ref_name; ?></div>
	<div class="Maenglish fleft" style="text-transform: none!important;"><span style="text-transform: none;font-weight: bold;"></span> <?php echo $key->ref_phone; ?></div>
	<div class="Maenglish fleft" style="text-transform: none!important;"><span style="text-transform: none;font-weight: bold;"></span> <?php echo $key->ref_email; ?></div>
	<div class="Maenglish fleft" style="text-transform: none!important;"><span style="text-transform: none;font-weight: bold;"></span> <?php echo $key->ref_address; ?></div>	
	<div class="Maenglish fleft" style="text-transform: none!important;"><span style="text-transform: none;font-weight: bold;"></span> <?php echo $key->ref_desg; ?></div>				
	</div>
	</div>		
			</td>
			<?php endforeach; ?>
		</tr>
	</table>
</div>


<!--------left box2 div ends-------->

<!--------Main Div class div ends-------->
</section>
</div>
</body>
</html>