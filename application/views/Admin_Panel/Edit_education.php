
  <?php
   //echo $all_education['ms_profile'];
   //die;
  $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside'); ?>

        <div class="right_col" role="main">
              
              <hr>
                  
                    <div id="wizard" class="form_wizard wizard_horizontal">

                           

                      <div id="step-1" style="margin-left:60px;">
                      <!-- step 1 -->
                      <div class="row">
                        <div class="col-sm-9">
                      
                        <h2 class="StepTitle"  style="font-family: serif;font-size:22px;margin-left:5px;">Edit Education Information</h2>
                        <hr><br>        

                        <form class="form-horizontal form-label-left" action="<?php echo base_url('ORB/edu_editproces/'); ?>" method="POST" enctype="multipart/form-data">
                          <div class="form-group">
                            <input type="hidden"   name="edu_id" ng-model="lname" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_education['edu_id']; ?>">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Education Name <span class="required" >*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="sec_name"  name="edu_name" ng-model="lname" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_education['edu_name']; ?>">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Institute <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="email" name="edu_inst" ng-model="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_education['edu_inst']; ?>">
                            </div>
                          </div>

                           <div class="form-group">
                            <label for="contact" class="control-label col-md-3 col-sm-3 col-xs-12">start date <span class="required" >*</span> </label>
                            <div class="col-md- col-sm-9 col-xs-12">
                              <input id="contact" required="" ng-model="contact" class="form-control col-md-7 col-xs-12" type="text"  placeholder="DD-MM-YYYY" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}"  name="edu_strdate" value="<?php echo $all_education['edu_strdate']; ?>">

                            </div>
                          </div>


                          <div class="form-group">
                             <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">end date <span class="required">*</span>
                            </label>
                             <div class="col-md-9 col-sm-9 col-xs-12">
                              <input  class="form-control col-md-7 col-xs-12" type="text"  placeholder="DD-MM-YYYY" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" id="input" name="edu_enddate" value="<?php echo $all_education['edu_enddate']; ?>">
                              <input type="checkbox" value="In Progress" name="edu_enddate"   onclick="check()" id="myCheck">&nbspIn Progress
                            </div>
                           
                          </div>
                        
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Specialization  <span class="required" >*</span> </label>
                            <div class="col-md-9 col-sm-9 col-xs-12 ">
                              <textarea class="form-control " ng-model="address" required="" name="edu_description" id="address"><?php echo $all_education['edu_description']; ?></textarea>
                              <br>
                            </div>
                          </div>
                           <div class="form-group">
                            <label for="start_date" class="control-label col-md-3 col-sm-3 col-xs-12">CGPA/Percenge</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <input type="text" value="<?php echo $all_education['edu_cgpa']; ?>" class="form-control col-md-7 col-xs-12"  name="edu_cgpa" pattern="[0-9]{0,2}.[0-9]{1,2}">
                            </div>

                         
                          </div>
                            

                          <input type="submit"  id="submit_personal" class="btn btn-success btn-md" value="Update">


                        </form>

                        
                      
                  </div>


                  </div> <!-- end row -->
                  </div>

</div>
</div>

          <div class="actionBar"><div class="msgBox"><div class="content"></div><a href="#" class="close">X</a></div><div class="loader">Loading</div><a href="" class="buttonNext btn btn-success">Next</a><a href="Education.php" class="buttonPrevious btn btn-primary">Previous</a></div>
          </div>

<script type="text/javascript">
      function check() {
    if(document.getElementById("myCheck").checked == true)
    {
    document.getElementById("input").disabled = true;
    document.getElementById("input").name= '';
    }
    else{
     document.getElementById("input").disabled = false;
     document.getElementById("input").name= 'edu_enddate';
    }
  }
/*
  function change_level()
  {
        var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET',"ajax?subtitle="+document.getElementById('level_tb').value,false);
    xmlHttp.send(null);
  document.getElementById('level').innerHTML=xmlHttp.responseText;

    //alert(xmlHttp.responseText);
    // alert(document.getElementById('level_td').value);
  
  }
*/
</script>
  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 