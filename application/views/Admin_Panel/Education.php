
  <?php $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside'); ?>

        <div class="right_col" role="main">
              <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3" style="display: inline-flex;">
                    
                    <a href="<?php echo base_url('ORB/info'); ?>"><button class="btn btn-success " style="min-width: 40%;"  name="button">Previous</button>
                    </a>

                    <a href="<?php echo base_url('ORB/workspace'); ?>"><button class="btn btn-primary " style="min-width: 40%;"  name="button">Next</button>
                    </a>
                </div>
                <a href="<?php echo base_url('ORB/edu_show/'.$this->session->userdata('master_id')); ?>" style="float:right;"><button class="btn btn-info" style="min-width: 40%;"  name="button">View</button></a>
              </div>
              <hr>
                  
                    <div id="wizard" class="form_wizard wizard_horizontal">

                           <ul class="wizard_steps">
                        <li>
                          <a href="">
                            <span class="step_no" style="background-color: silver;">1</span>
                            <span class="step_descr">
                                              Step 1<br />
                                              <small>Personal Information</small>
                                          </span>
                          </a>
                        </li>


                        <li>
                          <a href="#">
                            <span class="step_no" >2</span>
                            <span class="step_descr">
                                              Step 2<br />
                                              <small>Education</small>
                                          </span>
                          </a>
                        </li>

                        <li>
                          <a href="#step-3">
                            <span class="step_no" style="background-color: silver;">3</span>
                            <span class="step_descr">
                                              Step 3<br />
                                              <small>Work Experience</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-4">
                            <span class="step_no" style="background-color: silver;">4</span>
                            <span class="step_descr">
                                              Step 4<br />
                                              <small>Certification</small>
                                          </span>
                          </a>
                        </li>

                         <li>
                          <a href="#step-5">
                            <span class="step_no" style="background-color: silver;">5</span>
                            <span class="step_descr">
                                              Step 5<br />
                                              <small>Skills Profile</small>
                                          </span>
                          </a>
                        </li>


                         <li>
                          <a href="#step-7">
                            <span class="step_no" style="background-color: silver;">6</span>
                            <span class="step_descr">
                                              Step 6<br />
                                              <small>Languages</small>
                                          </span>
                          </a>
                        </li>

                         <li>
                          <a href="#step-8">
                            <span class="step_no" style="background-color: silver;">7</span>
                            <span class="step_descr">
                                              Step 7<br />
                                              <small>Hobbies & Games</small>
                                          </span>
                          </a>
                        </li>

                        <li>
                          <a href="#step-9">
                            <span class="step_no" style="background-color: silver;">8</span>
                            <span class="step_descr">
                                              Step 8<br />
                                              <small>References</small>
                                          </span>
                          </a>
                        </li>
                   </ul>

                      <div id="step-1" style="margin-left:60px;">
                      <!-- step 1 -->
                      <div class="row">
                      
                      
                        <div class="col-sm-9">
                      
                        <h2 class="StepTitle" style="font-family: serif;font-size:22px;margin-left:25px;">Education Profile</h2>
                <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                     <form action="<?php echo base_url('ORB/edu_insert'); ?>" class="form-horizontal form-label-left ng-pristine ng-invalid ng-invalid-required ng-valid-maxlength" method="post"  accept-charset="utf-8">
                           <span class="btn btn-danger" style="cursor: pointer; margin: 0px; padding-top: 0.5px;  height: 25px; float: right;" id="removeaddMore" my-attr="1">x</span>
                          <br>
                          <br>
                          <br>
                      <div id="addmore_entry">
                        
                          
                        <div id="addagain">
                          <div id="removeaddagain" >
                     

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="education">Education Name <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="education[]" ng-model="ngeducation"  name="education[]" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="institute_name">Institute Name <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="institute_name[]" name="institute_name[]"  required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="start_date" class="control-label col-md-3 col-sm-3 col-xs-12">Start Date*</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <input type="date" id="txtFromDate" class="form-control col-md-7 col-xs-12" type="month" required="required" ng-model="ngstart_date" name="start_date[]"  placeholder="dd-mm-yyyy">
                            </div>

                            <label for="end_date" class="control-label col-md-3 col-sm-3 col-xs-12">End Date 
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <input  type="date" id="input" name="end_date[]" class="form-control col-md-7 col-xs-12"    placeholder="dd-mm-yyyy">
                            
                            <input type="checkbox" value="In Progress" name="end_date[]"   onclick="check()" id="myCheck">&nbspIn Progress
                            </div>
                          </div>

                           <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"> Specialization  <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              
                              <textarea class="form-control col-md-7 col-xs-12" id="description[]" name="description[]" ng-model="ngdescription" maxlength="135" style="overflow-x:hidden;" required="required"></textarea>

                            </div>
                          </div>
                           <div class="form-group">
                            <label for="start_date" class="control-label col-md-3 col-sm-3 col-xs-12">CGPA/Percenge</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <input type="number" class="form-control col-md-7 col-xs-12"  name="CGPA[]" pattern="[0-9]{0,2}.[0-9]{1,2}">
                            </div>



                         
                          </div>
                           

                        
                        </div>
                        

                      </div>
                    </div>
                          <br>
                        <div class="col-md-12"  >
                         
                          <input type="submit" name="submit" class="btn btn-success" style="float: right;">
                           <span class="btn btn-primary" style="float: right; cursor: pointer; margin-bottom:10px; " id="addMore" ><i class="glyphicon glyphicon-plus"></i> Education</span>
                         </div>

                            

                        </form>
                      
                     
                  </div>
                  <!-- </div> -->

                  </div>
                </div>


                  </div> <!-- end row -->
                  </div>

</div>
</div>

        


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    function check() {
    if(document.getElementById("myCheck").checked == true)
    {
    document.getElementById("input").disabled = true;
    document.getElementById("input").name= '';
    }
    else{
     document.getElementById("input").disabled = false;
     document.getElementById("input").name= 'end_date[]';
    }
    }

  
 $(document).ready(function(){
    $("#addMore").click(function(){
        $("#addmore_entry").append(
          ' <hr style="font-size: 1vw; background-color:black;"><div id="addagain"><div id="removeaddagain"><div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="education">Education Name <span class="required">*</span></label><div class="col-md-9 col-sm-9 col-xs-12">  <input type="text" id="education[]" ng-model="ngeducation"  name="education[]" required="required" class="form-control col-md-7 col-xs-12"></div>                          </div><div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="institute_name">Institute Name <span class="required">*</span></label><div class="col-md-9 col-sm-9 col-xs-12">  <input type="text" id="institute_name[]" name="institute_name[]"  required="required" class="form-control col-md-7 col-xs-12"></div>                          </div>                          <div class="form-group"><label for="start_date" class="control-label col-md-3 col-sm-3 col-xs-12">Start Date*</label><div class="col-md-3 col-sm-3 col-xs-12">  <input type="text" id="txtFromDate" class="form-control col-md-7 col-xs-12" type="month" required="required" ng-model="ngstart_date" name="start_date[]" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" placeholder="dd-mm-yyyy"></div><label for="end_date" class="control-label col-md-3 col-sm-3 col-xs-12">End Date </label><div class="col-md-3 col-sm-3 col-xs-12">  <input  ng-model="ngend_date" id="input" name="end_date[]" class="form-control col-md-7 col-xs-12" text="text"  pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" placeholder="dd-mm-yyyy">         <input type="checkbox" value="In Progress" name="end_date[]"   onclick="check()" id="myCheck">&nbspIn Progress</div>                          </div>                           <div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"> Specialization  <span class="required">*</span></label><div class="col-md-9 col-sm-9 col-xs-12">             <textarea class="form-control col-md-7 col-xs-12" id="description[]" name="description[]" ng-model="ngdescription" maxlength="135" style="overflow-x:hidden;" required="required"></textarea></div>                          </div>                           <div class="form-group"><label for="start_date" class="control-label col-md-3 col-sm-3 col-xs-12">CGPA/Percenge</label><div class="col-md-3 col-sm-3 col-xs-12">  <input type="number" class="form-control col-md-7 col-xs-12"  name="CGPA[]  "></div>    </div>'

          );        

        return false; 
        
    }); 
});
 $( "#removeaddMore" ).click(function() {
  $( "#removeaddagain" ).remove();

});
</script>
  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 