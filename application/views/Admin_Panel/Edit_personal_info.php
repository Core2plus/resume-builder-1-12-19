
  <?php
   //echo $all_personals['ms_profile'];
   //die;
  $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside'); ?>

        <div class="right_col" role="main">
              
              <hr>
                  
                    <div id="wizard" class="form_wizard wizard_horizontal">

                           

                      <div id="step-1" style="margin-left:60px;">
                      <!-- step 1 -->
                      <div class="row">
                        <div class="col-sm-9">
                      
                        <h2 class="StepTitle"  style="font-family: serif;font-size:22px;margin-left:5px;">Edit Personal Information</h2>
                        <hr><br>        

                        <form class="form-horizontal form-label-left" action="<?php echo base_url('ORB/PI_editprocess'); ?>" method="POST" enctype="multipart/form-data">
                 <!-- <input type="hidden" name="hidden_text" value="<?php //echo $data->master_id; ?>">
              -->
              <input type="hidden" name="text_hide" value="<?php echo $all_personals['master_id']; ?>">
              <div class="form-group">
                <div class="row">
                  <label for="title" class="control-label col-md-3 col-sm-3 col-xs-12">Title <span class="required" >*</span> </label>
                      
                  <div class="col-md-3 col-sm-3 col-xs-12">
                      <!-- <div class="col-md-2"> -->
                       
                  <select name="title" id="title_tb" onchange="title_change()"  
                      class="form-control col-md-7 col-xs-12" required>
                      <option>Select title</option>
                    <?php 
                      $connect = mysqli_connect('localhost','root','','orb');
                      $q = "select * from title_table";

                      $result = mysqli_query($connect,$q);
                      //echo $q;
                      //die;
                      while($row = mysqli_fetch_assoc($result))
                      {
                      ?> 
                   <option value="<?php echo $row['title_id']; ?>"><?php echo $row['title_name'];
                    ?></option>
                    <?php
                  //  echo $id = $row['title_id'];
                  }
                  ?>  

                  </select>
                  </div>
                    <!-- </div> -->
       
                <div class="col-md-3 col-sm-3 col-xs-12" id="select-box2">
                <!-- <div class="col-md-2"> -->
                <select class="form-control col-md-7 col-xs-12" name="sub_title" id="sub_title" required>
                    <option value="">Select an Option</option>
                </select>

                </div>
                    <!-- </div> -->

                 <div class="col-md-3 col-sm-3 col-xs-12" id="lable1">
                    <!-- <div class="col-md-2"> -->
                    <select class="selectpicker form-control col-md-7 col-xs-12" name="level" id="level" standard title="Select an Option" autofocus="autofocus" required>
                    <option value="">Select Level</option>

                      <?php 
                      $connect = mysqli_connect('localhost','root','','orb');
                      $q = "select * from level_table";

                      $result = mysqli_query($connect,$q);
                      //echo $q;
                      //die;
                      while($row = mysqli_fetch_assoc($result))
                      {
                      ?> 
                   <option value="<?php echo $row['level_id']; ?>"><?php echo $row['level_name'];
                    ?></option>
                    <?php
                  //  echo $id = $row['title_id'];
                  }
                  ?>  



                    </select>
                </div>
          
                  <!-- </div> -->

                  </div>
                </div>


                           <div class="form-group">
                            <label for="title" class="control-label col-md-3 col-sm-3 col-xs-12">Select Your Image <span class="required" >*</span> </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                             
                              <label class="fileContainer">
                              Upload Your Picture here!
                              <input type="file" name="client_image" class="form-control col-md-9 col-xs-12"  id="user_image" accept="image/*"/>
                          </label>
                            </div>
                          </div>
        <div class="displayRadioButton"></div>

                          <div class="form-group">
                             <label class="control-label col-md-3 col-sm-3 col-xs-12" for="profile">IU Roll No* <span class="required"></span>
                            </label>
                             <div class="col-md-9 col-sm-9 col-xs-12">
                              <div>
                              <textarea class="form-control col-md-7 col-xs-12" required="" name="profile" maxlength="386" id="rollno" name="rollno"><?php echo $all_personals['ms_rollno']; ?></textarea>
                              </div>
                            </div>
                          </div>



                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">First Name <span class="required" >*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="name"  name="name" ng-model="fname" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_personals['ms_fname']; ?>">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Last Name <span class="required" >*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="sec_name"  name="sec_name" ng-model="lname" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_personals['ms_lname']; ?>">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="email" id="email" name="email" ng-model="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_personals['ms_email']; ?>">
                            </div>
                          </div>

                           <div class="form-group">
                            <label for="contact" class="control-label col-md-3 col-sm-3 col-xs-12">Contact <span class="required" >*</span> </label>
                            <div class="col-md- col-sm-9 col-xs-12">
                              <input id="contact" required="" ng-model="contact" class="form-control col-md-7 col-xs-12" type="text" name="contact" value="<?php echo $all_personals['ms_contact']; ?>">
                            </div>
                          </div>


                          <div class="form-group">
                             <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address <span class="required">*</span>
                            </label>
                             <div class="col-md-9 col-sm-9 col-xs-12">
                              <textarea class="form-control col-md-7 col-xs-12" ng-model="address" required="" name="address" id="address"><?php echo $all_personals['ms_address']; ?></textarea>
                            </div>
                          </div>
                        
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender <span class="required" >*</span> </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <div id="gender"  class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                  <input type="radio" ng-model="gender" name="gender" id="gender" value="male"> &nbsp; Male &nbsp;
                                </label>
                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                  <input type="radio" ng-model="gender" name="gender" id="gender" value="female"> Female
                                </label>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                              <input type="date" ng-model="date" id="datepicker" required="" name="date_of_birth" class="date-picker form-control col-md-7 col-xs-12" required="required" value="<?php echo $all_personals['ms_dob']; ?>">
                            </div>

                            
                            </div>
                          </div>

                          <input type="submit"  id="submit_personal" class="btn btn-success btn-md" value="Update">


                        </form>

                        <div id="personal_result"></div>
                      
                  </div>


                  </div> <!-- end row -->
                  </div>

</div>
</div>

          <div class="actionBar"><div class="msgBox"><div class="content"></div><a href="#" class="close">X</a></div><div class="loader">Loading</div><a href="" class="buttonNext btn btn-success">Next</a><a href="Education.php" class="buttonPrevious btn btn-primary">Previous</a></div>
          </div>

<script type="text/javascript">
  function title_change()
  {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET',"ajax?title="+document.getElementById('title_tb').value,false);
    xmlHttp.send(null);
  document.getElementById('sub_title').innerHTML=xmlHttp.responseText;
  //alert(xmlHttp.responseText);
  }
/*
  function change_level()
  {
        var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET',"ajax?subtitle="+document.getElementById('level_tb').value,false);
    xmlHttp.send(null);
  document.getElementById('level').innerHTML=xmlHttp.responseText;

    //alert(xmlHttp.responseText);
    // alert(document.getElementById('level_td').value);
  
  }
*/
</script>
  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 